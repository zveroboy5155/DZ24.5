#include <iomanip>
#include <iostream>
#include <map>
#include <ctime>
#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds
#include "windows.h"



void sol_3() {
	time_t end_time;
	std::tm diff;
	time_t cut_time;

	std::cout << "Entre duration of timer (format \"MIN:SEC\")" << std::endl;
	std::cin >> std::get_time(&diff, "%M:%S");
	std::cout << "START" << std::endl;
	cut_time = std::time(nullptr);
	end_time = cut_time + (time_t)(diff.tm_min * 60 + diff.tm_sec);

	while(cut_time < end_time){
		std::cout << (end_time - cut_time) / 60 << ":" << (end_time - cut_time) % 60 << std::endl;
        std::this_thread::sleep_for (std::chrono::seconds(1));
		//Sleep(1000);
		cut_time = std::time(nullptr);
	}
	std::cout << "DING! DING! DING!" << std::endl;

	return;
}