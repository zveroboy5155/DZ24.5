cmake_minimum_required(VERSION 3.16)
project(DZ_24_5)

set(CMAKE_CXX_STANDARD 11)

include_directories(.)

add_executable(DZ_24_5 DZ24.5.cpp
         task1.cpp task1.h task2.cpp task2.h
         task3.cpp task3.h)

