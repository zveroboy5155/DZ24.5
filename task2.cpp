#include <iomanip>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <Windows.h>

#include "task2.h"

struct p_data{
    std::string name;
    std::tm date;
};


void sol_2() {
    p_data cur_per;
    std::string cur_name;

	std::time_t t = std::time(nullptr);
	std::tm local = *(std::localtime(&t));
	std::tm current_t;
	std::vector<p_data> persons;
	//std::vector<std::tm> date_list;
	int min_diff = 0; //� ����

    SetConsoleCP(1251);// ��������� ������� �������� win-cp 1251 � ����� �����
    SetConsoleOutputCP(1251);
	//std::cout << std::put_time(&local, "%y/%m/%d") << std::endl;
	//std::cout << "tm_mon = " << local.tm_mon << std::endl;
	while (cur_per.name != "end") {
        //while(1){
        std::cout << "������� ��� �����" << std::endl;
        std::cin >> cur_per.name;
        if (cur_per.name == "end")
            continue;

        std::cout << "������� ���� �������� � ������� ���/�����/����" << std::endl;
        std::cin >> std::get_time(&cur_per.date, "%y/%m/%d");
        std::cout << "�������: " << std::put_time(&cur_per.date, "%m/%d") << std::endl;
        if (cur_per.date.tm_mon < local.tm_mon ||
            (cur_per.date.tm_mon == local.tm_mon && cur_per.date.tm_mday < local.tm_mday)) {
            std::cout << "���� �������� ��� ��� � ���� ����" << std::endl;
            continue;
        }
        if (persons.empty())
            min_diff = (cur_per.date.tm_mon - local.tm_mon) * 30 + (cur_per.date.tm_mday - local.tm_mday);

        if (((cur_per.date.tm_mon - local.tm_mon) * 30 + (cur_per.date.tm_mday - local.tm_mday)) > min_diff) {
            persons.push_back(cur_per);
        }else {
            persons.insert(persons.begin(), cur_per);
			min_diff = (current_t.tm_mon - local.tm_mon) * 30 + (current_t.tm_mday - local.tm_mday);
		}
	}
	std::cout << " ��� � ����� " << persons.size() << std::endl;
	if (persons.size() < 1) return;

	std::cout << " ��������� ���� �������� � " << persons[0].name << " "
		<< std::put_time(&persons[0].date, "%m/%d") << std::endl;
	
	for (int i = 1; i < persons.size(); i++) {
		if (persons[i].date.tm_mon == persons[i - 1].date.tm_mon &&
		    persons[i].date.tm_mday == persons[i - 1].date.tm_mday)
			std::cout << " ��������� ���� �������� � " << persons[i].name << " "
			<< std::put_time(&persons[i].date, "%m/%d") << std::endl;
		else break;
	}
	
}