#include "task1.h"

struct task {
	bool stop;
	time_t start;
	double duration;
	std::string text;
};

//std::vector<task> task_history;

task start_task() {
	task new_task;
	std::string task_text;

	std::cout << "Enter summary" << std::endl;
	//std::cout << "Введите текст задачи" << std::endl;
	std::cin >> task_text;
	new_task.text = task_text;
	new_task.stop = false;
	new_task.start = std::time(nullptr);
	new_task.duration = 0;
	
	return new_task;
}

bool stop_task(task &cur_task, std::vector<task> &task_history) {
	//std::vector<task>::iterator cur_task = task_history.end();

	if (cur_task.stop) return true;
	
	cur_task.stop = true;
	cur_task.duration = difftime(std::time(nullptr), cur_task.start);
	task_history.push_back(cur_task);

	return true;
}

bool print_tasks(task &cur_task, std::vector<task> &task_history) {

	
	if (!cur_task.stop) { 
		std::cout << "Current task: " << cur_task.text << std::endl;
		cur_task.duration = difftime(std::time(nullptr), cur_task.start);
		std::cout << "Duration: " << cur_task.duration <<" seconds" << std::endl;
		//std::cout << "Duration: " << std::put_time(std::localtime(&cur_task.duration), "%H:%M")<< std::endl;
		//std::cout << "Duration: " << std::put_time(std::localtime(&cur_task.start), "%H:%M") << std::endl;
	}

	for (auto it : task_history) {
		std::cout << "Old task: " << it.text << std::endl;
		//std::cout << "Текущая задача: " << it.text << std::endl;
		std::cout << "Duration: " << it.duration << " seconds" << std::endl;
		//std::cout << "Duration: " << asctime(std::localtime(&it.duration)) << std::endl;
		//std::cout << "Длительность: " << std::put_time( std::localtime(&it.duration), "%H:%M");
	}

	return true;
}

void sol_1() {
	std::string cmd("");
	task cur_task;
    std::vector<task> task_history;

	//setlocale(0, " ");
	SetConsoleCP(1251);// установка кодовой страницы win-cp 1251 в поток ввода
	SetConsoleOutputCP(1251);

	cur_task.stop = true;
	while (cmd != "exit") {
		//std::cout << "Введите одну из комманд\n:(begin, end, status, exit)" << std::endl;
		std::cout << "Enter command(begin, end, status, exit):" << std::endl;
		std::cin >> cmd;
		if (cmd == "begin" ) {
			if (!cur_task.stop) stop_task(cur_task, task_history);
			cur_task = start_task();
		}
		else if (cmd == "end") stop_task(cur_task, task_history);
		else if (cmd == "status") print_tasks(cur_task, task_history);
		else if (cmd == "exit") return;
		else std::cout << "Unknown command" << std::endl;
		//else std::cout << "Неизвестная комманда" << std::endl;
	}

	//std::cout << "Hello" << std::endl;
}
